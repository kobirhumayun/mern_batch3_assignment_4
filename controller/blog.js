const {
    blog
} = require('../model/blogModel')
const blogPost = (req, res, next) => {
    const post = new blog({
            body: req.body.body
        }

    )
    post.save()
    next()
}
const findApproved = async (req, res, next) => {
    const all = await blog.find({
        isApprved: true
    }, {
        body: 1,
        _id: 0
    })
    res.body = all
    next()
}
const pendingBlog = async (req, res, next) => {
    const all = await blog.find({
        isApprved: false
    }, {
        body: 1,
        _id: 0
    })
    res.body = all
    next()
}

const permission = async (req, res, next) => {
    const one = await blog.updateOne({
        _id: req.body._id
    }, {
        isApprved: true
    })
    res.body = one
    next()
}
const permissionCancel = async (req, res, next) => {
    const one = await blog.updateOne({
        _id: req.body._id
    }, {
        isApprved: false
    })
    res.body = one
    next()
}



module.exports = {
    blogPost,
    findApproved,
    pendingBlog,
    permission,
    permissionCancel
}