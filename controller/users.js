const mongoose = require('mongoose')
const {
    user
} = require('../model/userModel')
const {
    registrationvalidator
} = require('../validator/validator')
const allUsers = async (req, res, next) => {
    const all = await user.find({
        email: req.body.email
    })
    res.body = all
    next()
}
const registration = (req, res, next) => {
    try {
        const {
            error
        } = registrationvalidator.validate(req.body);
        if (error) {
            res.status(400).send({
                msg: "Validation error",
                error: error.details[0].message
            })
        } else {
            const person1 = new user({
                _id: new mongoose.Types.ObjectId,
                email: req.body.email,
                password: req.body.password,
                comment: []
            })
            person1.save()

            next()
        }
    } catch (error) {
        res.status(400).send({
            error
        })
    }


    // const person1 = new user({
    //     _id: new mongoose.Types.ObjectId,
    //     email: req.body.email,
    //     password: req.body.password,
    //     comment: []
    // })
    // person1.save()

    // next()
}
const updateComment = async (req, res, next) => {
    const one = await user.updateOne({
        _id: req.body._id
    }, {
        $push: {
            comment: req.body.comment
        }
    })
    res.body = one
    next()
}
module.exports = {
    allUsers,
    registration,
    updateComment
}