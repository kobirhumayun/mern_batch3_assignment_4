const mongoose = require('mongoose')
const {
    Schema
} = mongoose
const Blog = new Schema({
    body: String,
    isApprved: {
        type: Boolean,
        default: false

    }
})

const blog = mongoose.model('blog', Blog)
module.exports = {
    blog
}