const mongoose = require('mongoose')
const {
    Schema
} = mongoose
const User = new Schema({
    _id: Schema.Types.ObjectId,
    email: String,
    password: String,
    comment: [String]
})


const user = mongoose.model('users', User);
module.exports = {
    user
}