const express = require('express')
const route = express.Router()
const {
    blogPost,
    findApproved,
    pendingBlog,
    permission,
    permissionCancel
} = require('../controller/blog')
const {
    updateComment
} = require('../controller/users')

route.post('/post', blogPost, (req, res) => {
    res.status(200).send({
        res: "post successfully"
    })
})
route.get('/approved', findApproved, (req, res) => {
    res.status(200).send({
        res: res.body
    })
})
route.put('/approved-permission', permission, (req, res) => {
    res.status(200).send({
        res: res.body
    })
})
route.get('/pending', pendingBlog, (req, res) => {
    res.status(200).send({
        res: res.body
    })
})
route.put('/un-approved', permissionCancel, (req, res) => {
    res.status(200).send({
        res: res.body
    })
})
route.put('/comments', updateComment, (req, res) => {
    res.status(200).send({
        res: res.body
    })
})

module.exports = route