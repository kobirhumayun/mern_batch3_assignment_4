const express = require('express')
const route = express.Router()

const {
    allUsers,
    registration
} = require('../controller/users')
route.get('/all', allUsers, (req, res) => {
    res.status(200).send({
        msg: 'ok',
        res: res.body
    })
})
route.post('/registration', registration, (req, res) => {
    res.status(200).send({
        msg: "posted successfully",
        res: res.body
    })
})
module.exports = route