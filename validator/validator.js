const Joi = require('joi')
const registrationvalidator = Joi.object({
    email: Joi.string()
        .required(),
    password: Joi.string()
        .required()
})
module.exports = {
    registrationvalidator
}