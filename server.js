const express = require('express')
const app = express()
const mongoose = require('mongoose')
require('dotenv').config()
const port = process.env.PORT
const user = require('./route/user')
const blog = require('./route/blog')
app.use(express.json())
app.use('/user', user)
app.use('/blog', blog)

app.all('*', (req, res)=>{
    res.status(401).send('API not found')
})
app.listen(port, () => {
    console.log(`server is running on port ${port}`);
})
mongoose.connect('mongodb://localhost:27017/practice3')
